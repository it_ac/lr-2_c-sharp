﻿using System;

namespace LR_2_C_sharp
{
    class Program
    {
        static void Main(string[] args)
        {
            string str1 = "Строка первая";
            string str2 = "Стр_2-я";
            Print($"1. Строка str1 {CheckStr(str1, str2)} содержит все символы строки str2  ");
            Print("2. Строка с замененными символами: " + RemoveStr(str1, str2));
            Print("3. Str2 встречается в Str1: " + CheckStr2(str1, str2));

            Console.ReadKey();
        }

        static void Print(string str)
        {
            Console.WriteLine($"{str}");
        }

        /// <summary>
        /// Функция проверяет содержит ли Str1 все символы Str2.
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns></returns>
        static string CheckStr(string str1, string str2)
        {
            string checkStr = " ";
            if (str1.Equals(str2))
            {
                checkStr = "Да";
            }
            else
            {
                checkStr = "Нет не";
            }
            return checkStr;
        }

        /// <summary>
        /// Функция заменяет все символы в Str1 равные символам из Str2 на символ "#".
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns></returns>
        static string RemoveStr(string str1, string str2)
        {
            string k = " ";
            for (int i = 0; i < str1.Length; i++)
                for (int j = 0; j < str2.Length; j++)
                {
                    if (str1[i] == str2[j])
                    {
                        k = Convert.ToString(str1[i]);
                        str1 = str1.Replace(k, @"#");
                    }
                }
            return str1;
        }

        /// <summary>
        /// Функция определяет сколько раз Str2 встречается в Str1.
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns></returns>
        static string CheckStr2(string str1, string str2) 
        {
            int checkStrTrue = 0;
            for (int i = 0; i < str2.Length; i++)
                for (int j = 0; j < str1.Length; j++)
                {
                    if (str2[i] == str1[j])
                    {
                        checkStrTrue++;
                    }
                }
            return Convert.ToString(checkStrTrue);
        }
    }
}
